import React, { Component } from 'react';
import './styles/App.css';
import GalleryContainer from './components/GalleryContainer';
import Search from './components/Search';
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">ToltecoJS - Your Dream Gallery</h1>
        </header>
        <Search/>
        <GalleryContainer/>
      </div>
    );
  }
}

export default App;
