import React, { Component } from "react";
import { connect } from "react-redux";
import api from "../services/api";
import "../styles/App.css";
import Image from "./Image";

const mapStateToProps = state => ({
  searchQuery: state.searchQuery
});

const mapDispatchToProps = dispatch => ({});

class GalleryContainer extends Component {
  constructor(props) {
    super(props);
    this.state = { imagesData: [] };
  }

  componentDidMount() {
    this.fetchImages();
  }

  componentDidUpdate(prevProps) {
    if(this.props.searchQuery !== prevProps.searchQuery){
      this.fetchImages(this.props.searchQuery);
    }

  }

  render() {
    const { imagesData } = this.state;

    let images = imagesData.map(item => (
      <Image
        key={item.id}
        smallUrl={item.urls.small}
        fullUrl={item.urls.full}
        name={
          item.user.first_name +
          " " +
          (item.user.last_name ? item.user.last_name : "")
        }
        portfolioUrl={item.user.portfolio_url}
      />
    ));

    return (
      <div>
        <div className="gallery-container">{images}</div>
      </div>
    );
  }

  fetchImages(query="") {
    api.fetchRandomImages(20, query, "landscape", (error, data) => {
      if (error) {
        this.setState({
          loading: false
        });
        return console.log(error);
      }

      this.setState({
        loading: false,
        imagesData: data
      });
    });
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GalleryContainer);
