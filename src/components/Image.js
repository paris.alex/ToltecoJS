import React, { Component } from 'react';
import api from '../services/api';
import '../styles/App.css';


class Image extends Component {
    constructor(props) {
      super(props);
      this.state = {showInfo: false};
    }
  
    render(){
      const {smallUrl, fullUrl, name, portfolioUrl} = this.props;
      const {showInfo} = this.state;
      return (
        <div className="image" onMouseEnter={this.onEnterHandler.bind(this)} onMouseLeave={this.onLeaveHandler.bind(this)} >
            <img src={smallUrl} />
            {false && <div className="image-overlay"/>}
            {showInfo && <div className="image-info">
                <div className="image-info--name">
                    <a target="_blank" href={portfolioUrl}> {name} </a>
                </div>
                <a target="_blank" href={fullUrl}> <i className="fa fa-search-plus" /> </a>
            </div>
            }
        </div>
      );
    }

    onEnterHandler(e){
        this.setState({showInfo: true});
    }

    onLeaveHandler(e){
        this.setState({showInfo: false});
    }


  
}

export default Image;
