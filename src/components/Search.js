import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

import { searchQueryAction } from "../actions/SearchQueryActions";

import "../styles/App.css";

const mapStateToProps = state => ({});

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      searchQueryAction
    },
    dispatch
  );
}

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.state = { value: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <div className="">
        <form onSubmit={this.handleSubmit}>
          <input
            placeholder="Search for photos"
            type="search"
            name="searchQuery"
            value={this.state.value} 
            onChange={this.handleChange}
          />
          <input type="submit" value="Search" />
        </form>
      </div>
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.searchQueryAction(this.state.value);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
