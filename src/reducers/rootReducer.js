import { combineReducers } from 'redux';
import searchQuery from './searchQuery';
export default combineReducers({
 searchQuery
});