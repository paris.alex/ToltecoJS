export default (state = "", action) => {
    switch (action.type) {
     case 'SEARCH_QUERY_ENTERED':
      return action.payload
     default:
      return state
    }
   }