const API_HOST = "https://api.unsplash.com";
const API_KEY =
  "4bc7d15847a767ae774ed5effd308794a9b9c342020c1906446b30b5bada8c39";

class Api {
  constructor({ host }) {
    this.host = host;
  }

  fetchRandomImages(count = 10, query = "", orientation = "squarish", cb) {
    var request = new Request(
      this.host +
        `/photos/random?count=${count}&query=${query}&orientation=${orientation}`,
      {
        method: "GET",
        headers: new Headers({
          Authorization: "Client-ID " + API_KEY
        })
      }
    );

    fetch(request)
      .then(resp => {
        return resp.json();
      })
      .then(data => {
        return cb(null, data);
      })
      .catch(function(err) {
        console.log("ERROR", err);
        return cb(err, null);
      });
  }
}

export default new Api({ host: API_HOST });
