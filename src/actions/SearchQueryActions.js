export const searchQueryAction = (query) => dispatch => {
    dispatch({
     type: 'SEARCH_QUERY_ENTERED',
     payload: query
    })
   }